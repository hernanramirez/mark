package com.brightcomms.mark;

public interface NavigationDrawerCallbacks {
    void onNavigationDrawerItemSelected(int position);
}
