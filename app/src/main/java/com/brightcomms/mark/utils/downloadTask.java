package com.brightcomms.mark.utils;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.util.ByteArrayBuffer;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;


public class downloadTask {
    long startTime = 0;
    AsyncTask tsk;

    public downloadTask() {
    }

    public void run() {
        tsk = new downloadAtask().execute();
    }

    public void stop() {

        tsk.cancel(true);
    }

    class downloadAtask extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {
            try {
                //URL del archivo a descargar. No se va a descargar completamente, así que no importa que sea muy pesado.
                //String DownloadUrl = "http://speedtest.mediaserv.net/speedtest/random4000x4000.jpg";
                //String DownloadUrl = "http://speedtest1med.tigo.co/speedtest/random1000x1000.jpg";
                //String DownloadUrl = "http://demo.cida.gob.ve/random1000x1000.jpg";
                String DownloadUrl = "http://markmobile.brightcomms.com/static/filesDownload/random750x750.jpg";
                //String DownloadUrl = "http://meidiula.org/random4000x4000.jpg";

                URL url = new URL(DownloadUrl); //

                //Empezamos la cuenta del tiempo
                container.setStartTime(System.currentTimeMillis());
                Log.d("DownloadManager", "download begining: " + startTime);
                Log.d("DownloadManager", "download url:" + url);
                URLConnection ucon = null;
           /* Open a connection to that URL. */
                try {
                    ucon = url.openConnection();
                } catch (Exception e) {
                    Log.v("EXCEPTION", e.toString());
                }
                //Define InputStreams to read from the URLConnection.
                if (ucon != null) {
                    InputStream is = ucon.getInputStream();
                    BufferedInputStream bis = new BufferedInputStream(is);

                    //Empezamos a recibir datos de la web y a incrementar el indicador del número de datos leídos (container.getSize / setSize)
                    ByteArrayBuffer baf = new ByteArrayBuffer(1024);
                    int current = 0;
                    String ratevalue = "";
                    while ((current = bis.read()) != -1) {
                        container.setSize(container.getSize() + 1);
                        if (isCancelled()) break;

                    }


                    container.setStop(true);


                }
            } catch (IOException e) {
                Log.d("DownloadManager", "Error: " + e);
            }
            return null;
        }


    }


}