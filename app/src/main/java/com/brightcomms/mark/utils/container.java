package com.brightcomms.mark.utils;


import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by Juan on 21/05/2015.
 * Esta clase simplemente es un contenedor de variables para solucionar problemas de concurrencia
 * y de intercambio de variables entre hilos.
 * Por eso son casi todos los métodos syncronized
 */
public class container {
    public static long startTime = 0;
    public static double pico = 0;
    public static double average = 0;
    public static double num=0;
    public static ArrayList<Double> ar = new ArrayList<>();
    public static int times=0;
    public static String uploadSpeed="ERROR: TIMEOUT IN UPLOAD SPEED";
    public static String downloadSpeed="ERROR: TIMEOUT IN DOWNLOAD SPEED";


    public static synchronized boolean isStop() {
        return stop;
    }

    public static synchronized void setStop(boolean stop) {
        container.stop = stop;
    }

    public static boolean stop = false;
    private static String currentSpeed="";
    public static synchronized long getStartTime() {
        return startTime;
    }
    public static synchronized double getPico() {
        return pico;
    }

    public static synchronized void setPico(double pico) {
        container.pico=pico;
    }

    public static synchronized void setStartTime(long startTime) {
        container.startTime = startTime;
    }

    public static synchronized long getEndTime() {
        return endTime;
    }

    public static synchronized void setEndTime(long endTime) {
        container.endTime = endTime;
    }

    public static synchronized double getSize() {
        return size;
    }

    public static synchronized void setSize(double size) {
        container.size = size;
    }

    public static long endTime = 0;
    public static AtomicBoolean downloadFinalized = new AtomicBoolean(false);
    public static double size = 0;

    public static void getDownloadSpeed(){
        setEndTime(System.currentTimeMillis()); //maybe

        double rate = (((getSize() / 1024) / ((getEndTime() - getStartTime()) / 1000)) * 8);
        rate = Math.round(rate * 100.0) / 100.0;
        String ratevalue="";
        if (rate > 1000) {
            ratevalue = String.valueOf(rate / 1024).concat(" Mbps");

        }
        else
            ratevalue = String.valueOf(rate).concat(" Kbps");

        if(ratevalue.contains("007199254740992E13"))
            rate=0;

        if (getPico() < rate){
            setPico(rate);
        }
        if(rate!=0){
            average+=rate;
            num++;
            ar.add(rate);
        }
        //Log.d("DownloadManager", "download speed: " + ratevalue);
        setSize(0);
        setCurrentSpeed(ratevalue);
        setStartTime(System.currentTimeMillis());
    }


    public static synchronized String getCurrentSpeed() {
        return currentSpeed;
    }

    public static synchronized void setCurrentSpeed(String currentSpeed) {
        container.currentSpeed = currentSpeed;
    }
}
