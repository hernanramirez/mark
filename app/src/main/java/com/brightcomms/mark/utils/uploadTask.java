package com.brightcomms.mark.utils;

import android.os.AsyncTask;
import android.util.Log;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicBoolean;


public class uploadTask {
    TimerTask timerTask;
    Socket echoSocket;
    Timer t;
    PrintWriter out;
    private DataInputStream in;
    private AtomicBoolean enter;
    private AsyncTask ct;

    public uploadTask() {
        enter = new AtomicBoolean(true);
        t = new Timer(false);
        timerTask = new TimerTask() {
            @Override
            public void run() {
                enter.set(false);

                if (echoSocket != null) {
                    out.print('*');
                    out.flush();
                }
                ct = new checkTask().execute();
                t.cancel();
            }
        };
    }

    public void run() {
        new uploadAtask().execute();

    }

    class checkTask extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... params) {
            try {
                String userInput;
                boolean end = false;
                int bytesRead;
                byte[] messageByte = new byte[1000];
                String dataString = "";

                while (!end) {
                    bytesRead = in.read(messageByte);
                    dataString += new String(messageByte, 0, bytesRead);
                    if (dataString.contains("*")) //Esto significa que es el fin de la respuesta del servidor
                    {
                        end = true;
                        container.uploadSpeed = dataString;
                        Log.v("UPLOAD SPEED", dataString);
                        enter.set(false);
                    }
                }
                if (echoSocket != null) { //Mandamos un asterisco al servidor para indicar que finalizamos la conexión
                    out.print('*');
                    out.flush();
                    echoSocket.close();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    class uploadAtask extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {
            echoSocket = null;
            char[] bytearray = new char[1024]; //rellenamos una cadena de texto del tamaño máximo del bufer del servidor (1024 bytes)
            for (int i = 0; i < 1024; i++)
                bytearray[i] = 'a';
            try {
                echoSocket = new Socket("50.112.17.135", 5991); //Se conecta al servidor
                //echoSocket = new Socket("75.126.113.168", 5991);
                t.schedule(timerTask, 11000, 110000); // En 11 segundos se pone la alarma para recoger los resultados de la velocidad. Cuando salta la alarma, se dejan de enviar datos
                in = new DataInputStream(echoSocket.getInputStream());
                out = new PrintWriter(echoSocket.getOutputStream(), true);
                //ct = new checkTask().execute(); //
                while (enter.get() == true) { //Mientras no salte la alarma de los 11 segundos
                    out.print(bytearray);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;

        }
    }
}
