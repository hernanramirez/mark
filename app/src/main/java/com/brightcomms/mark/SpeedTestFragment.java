package com.brightcomms.mark;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.brightcomms.mark.utils.GPSTracker;
import com.brightcomms.mark.utils.container;
import com.brightcomms.mark.utils.downloadTask;
import com.brightcomms.mark.utils.uploadTask;
import com.cardiomood.android.controls.gauge.SpeedometerGauge;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPSClient;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SpeedTestFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SpeedTestFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SpeedTestFragment extends Fragment {

    public double latitude;
    public double longitude;
    public String[] coordenadas;

    private Button btnUpload;

    // GPSTracker class
    GPSTracker gps;

    TextView txtDownloadSpeed;
    TextView txtUploadSpeed;
    TextView txtLatency;
    TextView txtPing;
    TextView txtStatus;
    TextView titleLeft;
    TextView titleRight;

    Button bntTest;
    Button bntConnectionSave;

    ProgressBar progresBarEstatus;

    private SpeedometerGauge speedometer;

    private double uploadRate;

    private double downloadRate;

    private long latencyRate;

    private String pingRate;

    protected ArrayList<Button> buttons = new ArrayList<Button>();

    public static List<Double> totalDescargado = new ArrayList<Double>();
    public static List<Double> totalSubido = new ArrayList<Double>();

    private ExecutorService pool;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    ProgressDialog progressBar;


    Timer t;

    downloadTask c;
    Timer u;
    TimerTask timerTask;
    TimerTask upload;
    uploadTask a = null;

    final Handler handler = new Handler();

    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SpeedTestFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SpeedTestFragment newInstance(String param1, String param2) {
        SpeedTestFragment fragment = new SpeedTestFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public SpeedTestFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_speed_test, container, false);
        titleLeft = (TextView) view.findViewById(R.id.txtTitleLeft);
        titleRight = (TextView) view.findViewById(R.id.txtTitleRight);
        txtDownloadSpeed = (TextView) view.findViewById(R.id.txtDownloadSpeed);
        txtUploadSpeed = (TextView) view.findViewById(R.id.txtUploadSpeed);
        txtLatency = (TextView) view.findViewById(R.id.txtLatency);
        txtPing = (TextView) view.findViewById(R.id.txtPing);
        txtStatus = (TextView) view.findViewById(R.id.txtStatus);
        progresBarEstatus = (ProgressBar) view.findViewById(R.id.progressBarTestConnection);

        bntTest = (Button) view.findViewById(R.id.btnTest);

        //bntConnectionSave.setEnabled(false);

        // Customize SpeedometerGauge
        speedometer = (SpeedometerGauge) view.findViewById(R.id.speedometer);

        // Add label converter
        /*
        speedometer.setLabelConverter(new SpeedometerView.LabelConverter() {
            @Override
            public String getLabelFor(double progress, double maxProgress) {
                return String.valueOf((int) Math.round(progress));
            }
        });
        */
        // configure value range and ticks
        speedometer.setMaxSpeed(10);
        speedometer.setMajorTickStep(1);
        speedometer.setMinorTicks(0);

        // Configure value range colors
        //speedometer.addColoredRange(0, 30, Color.GREEN);
        //speedometer.addColoredRange(40, 50, Color.YELLOW);
        //speedometer.addColoredRange(51, 100, Color.RED);

        pool = Executors.newSingleThreadExecutor();


        titleRight.setText(checkActiveNetworkState(view.getContext()));


        // gps position
        gps = new GPSTracker(view.getContext());

        // check if GPS enabled
        if (gps.canGetLocation()) {

            latitude = gps.getLatitude();
            longitude = gps.getLongitude();

            titleLeft.setText("Position:" + String.valueOf(latitude) + "," + String.valueOf(longitude));

        } else {
            gps.showSettingsAlert();
        }

        buttons.add((Button) view.findViewById(R.id.btnTest));


        for (Button btn : buttons) btn.setOnClickListener(listener);


        return view;
    }

    protected View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            switch (view.getId()) {
                case R.id.btnTest:
                    titleRight.setText(checkActiveNetworkState(view.getContext()));

                    // create and display a new ProgressBarDialog
                    progressBar = new ProgressDialog(view.getContext());
                    progressBar.setCancelable(true);
                    progressBar.setMessage("Latency calc ...");
                    progressBar.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                    progressBar.setProgress(0);
                    progressBar.setMax(100);
                    progressBar.show();

                    new testLatency().execute("URL");

                    u = new Timer(false);
                    t = new Timer(false);

                    //
                    c = new downloadTask();
                    c.run();
                    container.getDownloadSpeed();
                    container.times = 0;


                    timerTask = new TimerTask() {
                        @Override
                        public void run() {
                            if (container.times == 7) { // Esta tarea se ejecuta cada 1,5 segundos. Queremoos tener la velocidad en un máximo de 10, así que solo vamos a dejar que se ejecute 7 veces. Cuando esto suceda, habrán pasado 10(.5) segundos
                                Collections.sort(container.ar); //Ordena la lista de datos de bajada para calcular la mediana
                                Log.d("DownloadManager", "Velocidad mediana de bajada: " + container.ar.get(container.ar.size() / 2) / 1024 + " Mbps");
                                Log.d("DownloadManager", "Velocidad media de bajada: " + (container.average / container.num) / 1024 + " Mbps");
                                //Guardamos la velocidad media de bajada en la clase container

                                container.downloadSpeed = (container.average / container.num) / 1024 + " Mbps";

                                Log.d("DownloadManager", "Velocidad máxima de bajada: " + container.getPico() / 1024 + " Mbps");

                                //Inicializa la tarea de medición de velocidad de subida
                                container.downloadFinalized.set(true);
                                c.stop();
                                t.cancel(); //Para el temporizador

                                //Para la tarea de subida.
                                //Aqui se podria iniciar la tarea upload.

                                handler.post(new Runnable() {
                                    public void run() {
                                        txtDownloadSpeed.setText(String.valueOf(round((container.average / container.num) / 1024, 2)));
                                        progressBar.setProgress(75);
                                        progressBar.setMessage("Test upload ...");
                                        c = null;
                                        t.purge();
                                        //container.times = 0;
                                        //container.setStop(false);

                                    }
                                });


                            } else {
                                container.times++;
                                String currentSpeed = container.getCurrentSpeed();
                                if (!currentSpeed.equals(""))
                                    Log.d("DownloadManager", "download speed: " + container.getCurrentSpeed()); //Muestra la velocidad actual de bajada
                                container.getDownloadSpeed();
                            }
                        }

                    };


                    upload = new TimerTask() {
                        @Override
                        public void run() {
                            if (container.downloadFinalized.get() && a == null) {
                                Log.d("DownloadManager", "Calculando subida...");

                                a = new uploadTask();
                                a.run();

                            } else if (container.uploadSpeed.contains("*")) {
                                //AQUI TAMBIEN PODRIAMOS MOSTRAR LA VELOCDAD MEDIA DE BAJADA: container.downloadSpeed
                                Log.d("DownloadManager", "Velocidad de subida: " + container.uploadSpeed);

                                upload.cancel();
                                u.cancel();
                                a = null;


                                handler.post(new Runnable() {
                                    public void run() {
                                        txtUploadSpeed.setText(container.uploadSpeed);
                                        progressBar.setProgress(100);
                                        progressBar.dismiss();
                                        container.downloadFinalized.set(false);
                                        container.uploadSpeed = "";

                                    }
                                });

                            }
                        }
                    };
                    t.schedule(timerTask, 1500, 1500); //Programa la tarea para que cada 1'5 segundos se muestre la velocidad de bajada.
                    u.schedule(upload, 500, 500);
                    break;
            }
        }
    };





    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }













    public static void Downlo() throws InterruptedException {

        //System.out.println("Se inicia la descarga");


        long startTime = System.currentTimeMillis();
        ExecutorService executor = Executors.newFixedThreadPool(5);
        for (int i = 0; i < 5; i++) {
            Runnable worker = new DownloadWorkerThread("command" + i);
            executor.execute(worker);
        }
        executor.shutdown();
        while (!executor.isTerminated()) {
        }
        executor.awaitTermination(Long.MAX_VALUE, TimeUnit.MILLISECONDS);

        double totalArray= 0;
        for(double i : totalDescargado){
            totalArray += i;
        }

        //double totalGeneral = (totalArray*8) / ((System.currentTimeMillis() - startTime) / 1000);
        double totalGeneral = (totalArray) / (System.currentTimeMillis() - startTime);

        System.out.println("Finished all threads");
        System.out.println("Tota time bps: "+ String.valueOf(totalGeneral));
        System.out.println("Tota time: Kbps "+ String.valueOf(totalGeneral/1000));
        System.out.println("Tota time: Mbps: "+ String.valueOf(totalGeneral/1000/1000));
        System.out.println("Tota Download: Mbps: "+ String.valueOf(totalArray/1000/1000));



    }

    public void Uplo() throws InterruptedException {

        //System.out.println("Se inicia la descarga");


        long startTime = System.currentTimeMillis();
        ExecutorService executorUpload = Executors.newFixedThreadPool(4);
        for (int i = 0; i < 3; i++) {
            Runnable worker = new UploadWorkerThread("command" + i);
            executorUpload.execute(worker);
        }
        executorUpload.shutdown();
        while (!executorUpload.isTerminated()) {
        }
        executorUpload.awaitTermination(Long.MAX_VALUE, TimeUnit.MILLISECONDS);

        double totalArray= 0;
        for(double i : totalSubido){
            totalArray += i;
        }

        //double totalGeneral = (totalArray*8) / ((System.currentTimeMillis() - startTime) / 1000);
        double totalGeneral = (totalArray) / (System.currentTimeMillis() - startTime);

        System.out.println("Finished all threads");
        System.out.println("Tota time bps: "+ String.valueOf(totalGeneral));
        System.out.println("Tota time: Kbps "+ String.valueOf(totalGeneral/1000));
        System.out.println("Tota time: Mbps: "+ String.valueOf(totalGeneral/1000/1000));
        System.out.println("Tota time upload: Mbps: "+ String.valueOf(totalArray/1000/1000));

    }

    private String checkActiveNetworkState(Context context) {


        String simCountry;
        String simOperatorCode;
        String simOperatorName = "";
        String simSerial;

        ConnectivityManager mConnectivityManager;
        mConnectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        TelephonyManager mTelephonyManager;
        mTelephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

        int simState = mTelephonyManager.getSimState();
        switch (simState) {
            case (TelephonyManager.SIM_STATE_ABSENT):
                break;
            case (TelephonyManager.SIM_STATE_NETWORK_LOCKED):
                break;
            case (TelephonyManager.SIM_STATE_PIN_REQUIRED):
                break;
            case (TelephonyManager.SIM_STATE_PUK_REQUIRED):
                break;
            case (TelephonyManager.SIM_STATE_UNKNOWN):
                break;
            case (TelephonyManager.SIM_STATE_READY): {
                // Get the SIM country ISO code
                simCountry = mTelephonyManager.getSimCountryIso();
                // Get the operator code of the active SIM (MCC + MNC)
                simOperatorCode = mTelephonyManager.getSimOperator();
                // Get the name of the SIM operator
                simOperatorName = mTelephonyManager.getSimOperatorName();
                // -- Requires READ_PHONE_STATE uses-permission --
                // Get the SIM’s serial number
                simSerial = mTelephonyManager.getSimSerialNumber();
            }
        }

        String networkState = "Null";


        if (mConnectivityManager != null) {
            NetworkInfo mNetworkInfo = mConnectivityManager.getActiveNetworkInfo();
            if (mNetworkInfo == null) {
                networkState = "Null";
                return networkState;
            } else {
                if (mNetworkInfo.getType() == ConnectivityManager.TYPE_WIFI) {
                    networkState = "WIFI";
                    WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
                    WifiInfo connectionInfo = wifiManager.getConnectionInfo();
                    if (connectionInfo != null && !TextUtils.isEmpty(connectionInfo.getSSID())) {
                        simOperatorName = connectionInfo.getSSID();
                    }
                } else if (mNetworkInfo.getType() == ConnectivityManager.TYPE_MOBILE) {

                    switch (mNetworkInfo.getSubtype()) {
                        case TelephonyManager.NETWORK_TYPE_EDGE:
                            networkState = "MOBILE EDGE";
                            break;
                        case TelephonyManager.NETWORK_TYPE_UMTS:
                            networkState = "MOBILE UMTS";
                            break;
                        case TelephonyManager.NETWORK_TYPE_GPRS:
                            networkState = "MOBILE GPRS";
                            break;
                        case TelephonyManager.NETWORK_TYPE_CDMA:
                            networkState = "MOBILE CDMA";
                            break;
                        case TelephonyManager.NETWORK_TYPE_EVDO_0:
                            networkState = "MOBILE EVDO_0";
                            break;
                        case TelephonyManager.NETWORK_TYPE_EVDO_A:
                            networkState = "MOBILE EVDO_A";
                            break;
                        case TelephonyManager.NETWORK_TYPE_1xRTT:
                            networkState = "MOBILE 1xRTT";
                            break;
                        case TelephonyManager.NETWORK_TYPE_HSDPA:
                            networkState = "MOBILE HSDPA";
                            break;
                        case TelephonyManager.NETWORK_TYPE_HSPA:
                            networkState = "MOBILE HSPA";
                            break;

                        case TelephonyManager.NETWORK_TYPE_EVDO_B:
                            networkState = "MOBILE EVDO_B";
                            break;

                        case TelephonyManager.NETWORK_TYPE_LTE:
                            networkState = "MOBILE LTE";
                            break;

                        case TelephonyManager.NETWORK_TYPE_HSUPA:
                            networkState = "MOBILE HSUPA";
                            break;


                        case TelephonyManager.NETWORK_TYPE_UNKNOWN:
                            networkState = "MOBILE UNKNOWN";
                            break;
                        default:
                            break;
                    }

                }
            }
        } else {
            networkState = "Null";
        }

        return simOperatorName + " | " + networkState;
    }




    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }


    // Async Task Class
    class testLatency extends AsyncTask<String, String, String> {

        // Show Progress bar before downloading Music
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setProgress(0);
            progressBar.setMessage("Latency calc...");
            //progresBarEstatus.setProgress(0);
            //progresBarEstatus.setMax(100);
            //txtStatus.setText("Test latency");
            //bntTest.setEnabled(false);


        }

        // Download Music File from Internet
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {


                String host = "sysdigital.com.ve";
                int timeOut = 3000;
                long[] time = new long[5];
                Boolean reachable;

                //for (int i = 0; i < 5; i++) {
                    long BeforeTime = System.currentTimeMillis();
                    reachable = InetAddress.getByName(host).isReachable(timeOut);
                    long AfterTime = System.currentTimeMillis();
                    Long TimeDifference = AfterTime - BeforeTime;
                    //time[i] = TimeDifference;
                    Log.d("ANDRO_ASYNC", "TimeDifference: " + TimeDifference);
                    latencyRate = TimeDifference;
                    //progresBarEstatus.setProgress((int) ((i * 100) / 5));
                //}


            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }
            return null;
        }

        // While Downloading Music File
        protected void onProgressUpdate(String... progress) {
            // Set progress percentage
            //prgDialog.setProgress(Integer.parseInt(progress[0]));
        }

        // Once Music File is downloaded
        @Override
        protected void onPostExecute(String file_url) {
            txtLatency.setText(String.valueOf("Latency: "+latencyRate ));
            txtStatus.setText("");
            new testPings().execute("URL");
        }
    }

    // Async Task Class
    class testPings extends AsyncTask<String, String, String> {

        // Show Progress bar before downloading Music
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setProgress(25);
            progressBar.setMessage("Ping calc ...");
            // Shows Progress Bar Dialog and then call doInBackground method
            //progresBarEstatus.setProgress(25);
            //progresBarEstatus.setMax(100);
            //txtStatus.setText("Test ping");
        }

        // Download Music File from Internet
        @Override
        protected String doInBackground(String... f_url) {
            int count = 0;
            try {

                String host = "69.73.144.31";
                //String host = "127.0.0.1";

                String pingResult = "";
                //  System.out.println("Came in pingTest");
                String pingCmd = "/system/bin/ping -c 3 " + host;

                try {
                    Runtime r = Runtime.getRuntime();

                    Process p = r.exec(pingCmd);
                    BufferedReader in = new BufferedReader(new
                            InputStreamReader(p.getInputStream()));
                    String inputLine;

                    Pattern pattern = Pattern.compile("time=(\\d+)ms");
                    Matcher m = null;


                    while ((inputLine = in.readLine()) != null) {
                        /*
                        count += 33;
                        progresBarEstatus.setProgress(count);
                        //System.out.println(inputLine);
                        if (inputLine.contains("rtt")){
                            pingResult += inputLine + "\n";
                            pingRate = inputLine;
                            Log.d("ANDRO_ASYNC", "Ping: " + pingRate);
                        }
                        m = pattern.matcher(inputLine);
                        if (m.find()) {
                            System.out.println(m.group(1));
                            pingRate = inputLine;
                        }
                        */
                        pingResult += inputLine;
                        //Log.d("ANDRO_ASYNC", "Ping: " + inputLine);
                    }
                    pingResult = pingResult.substring(pingResult.lastIndexOf("rtt min/avg/max/mdev"));
                    pingResult = pingResult.substring(23);
                    pingResult = pingResult.substring(pingResult.indexOf("/") + 1);
                    Double x = Double.parseDouble(pingResult.substring(0,pingResult.indexOf("/")));
                    pingRate = String.valueOf(x);
                    Log.d("ANDRO_ASYNC", "pingResult: " + String.valueOf(x));
                    in.close();
                } catch (IOException e) {
                    System.out.println(e);
                }

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }
            return null;
        }

        // Once Music File is downloaded
        @Override
        protected void onPostExecute(String file_url) {

            txtPing.setText("Ping: " + pingRate + " ms");
            //new testBandwidthDownload().execute("URL");
        }
    }


    public static class DownloadWorkerThread implements Runnable {

        private String command;

        public DownloadWorkerThread(String s){
            this.command=s;
        }

        @Override
        public void run() {
            System.out.println(Thread.currentThread().getName() + " Start. Command = " + command);
            processCommand();
            System.out.println(Thread.currentThread().getName() + " End.");
        }

        private void processCommand() {
            /*
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            */

            //downloadFile("http://markmobile.brightcomms.com/static/filesDownload/random3500x3500.jpg", "/sdcard/xxx");
            if (new String("command0").equals(command)) {
                downloadFile("http://speedcheck.setarnet.aw/speedtest/random750x750.jpg", "/sdcard/xxx");
            } else if(new String("command1").equals(command)) {
                downloadFile("http://speedcheck.setarnet.aw/speedtest/random1000x1000.jpg", "/sdcard/xxx");
            }  else if(new String("command2").equals(command)) {
                downloadFile("http://speedcheck.setarnet.aw/speedtest/random1500x1500.jpg", "/sdcard/xxx");
            }  else if(new String("command3").equals(command)) {
                downloadFile("http://speedcheck.setarnet.aw/speedtest/random2000x2000.jpg", "/sdcard/xxx");
            } else if(new String("command4").equals(command)) {
                downloadFile("http://speedcheck.setarnet.aw/speedtest/random3500x3500.jpg", "/sdcard/xxx");
            } else if(new String("command5").equals(command)) {
                downloadFile("http://speedcheck.setarnet.aw/speedtest/random4000x4000.jpg", "/sdcard/xxx");
            }



        }

        @Override
        public String toString(){
            return this.command;
        }
    }

    public static void downloadFile(String remotePath, String localPath) {
        BufferedInputStream in = null;
        FileOutputStream out = null;

        try {
            URL url = new URL(remotePath);
            URLConnection conn = url.openConnection();
            int size = conn.getContentLength();

            if (size < 0) {
                System.out.println("Could not get the file size");
            } else {
                System.out.println("File size: " + size);
            }

            in = new BufferedInputStream(url.openStream());
            out = new FileOutputStream(localPath);
            byte data[] = new byte[1024];
            int count;
            double sumCount = 0.0;
            long sTime = System.currentTimeMillis();

            while ((count = in.read(data, 0, 1024)) != -1) {
                //out.write(data, 0, count);
                long eTime = System.currentTimeMillis();

                sumCount += count;

                if ( ( (eTime- sTime)/1000 ) >= 10 ) {

                    totalDescargado.add(sumCount);
                    break;
                }

                if (size > 0) {
                    //System.out.println("Percentace: " + (sumCount / size * 100.0) + "%");
                }
            }

        } catch (MalformedURLException e1) {
            e1.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        } finally {
            if (in != null)
                try {
                    in.close();
                } catch (IOException e3) {
                    e3.printStackTrace();
                }
            if (out != null)
                try {
                    out.close();
                } catch (IOException e4) {
                    e4.printStackTrace();
                }
        }
    }



    public class UploadWorkerThread implements Runnable {

        private String command;

        public UploadWorkerThread(String s){
            this.command=s;
        }

        @Override
        public void run() {
            System.out.println(Thread.currentThread().getName() + " Start. Command = " + command);
            try {
                processCommand();
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName() + " End.");

        }

        private void processCommand() throws IOException {
            /*
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            */


            if (new String("command0").equals(command)) {
                uploadFile(1);
            } else if(new String("command1").equals(command)) {
                uploadFile(2);
            }  else if(new String("command2").equals(command)) {
                uploadFile(3);
            }  else if(new String("command3").equals(command)) {
                uploadFile(4);
            } else if(new String("command4").equals(command)) {
                uploadFile(5);
            } else if(new String("command5").equals(command)) {
                uploadFile(6);
            }

        }

        @Override
        public String toString(){
            return this.command;
        }
    }

    public void uploadFile(Integer sizeUpload) throws IOException {

        int count;
        try {

            //new ftp client
            FTPClient ftp = new FTPClient();
            //try to connect
            ftp.connect("ftp.sysdigital.com.ve");
            //login to server
            if (!ftp.login("mark@sysdigital.com.ve", "mark2015q")) {
                ftp.logout();
                //Toast.makeText(getApplicationContext(), "no se conecta", Toast.LENGTH_LONG).show();

            }

            ftp.setFileType(FTPSClient.BINARY_FILE_TYPE);
            ftp.enterLocalPassiveMode();
            ftp.setBufferSize(4096);

            // APPROACH #2: uploads second file using an OutputStream
            File secondLocalFile = new File("/sdcard/1mb.jpg");
            String secondRemoteFile = "/1mbX.jpg";

            InputStream inputStream = getResources().openRawResource(R.raw.random750x750);

            if (sizeUpload== 1){
                inputStream = getResources().openRawResource(R.raw.random750x750);
                secondRemoteFile = String.valueOf(sizeUpload)+"_750.jpg";
            }else if (sizeUpload== 2){
                inputStream = getResources().openRawResource(R.raw.random1000x1000);
                secondRemoteFile = String.valueOf(sizeUpload)+"_1000.jpg";
            }

            OutputStream outputStream = ftp.storeFileStream(secondRemoteFile);
            byte[] bytesIn = new byte[4096];
            int read = 0;
            int bytesUploadedSet = 0;
            int pTotal = 0;

            long startTime = System.currentTimeMillis();
            long endTime = 1;

            double sumCount = 0.0;
            long sTime = System.currentTimeMillis();

            while ((read = inputStream.read(bytesIn)) != -1) {

                long eTime = System.currentTimeMillis();

                outputStream.write(bytesIn, 0, read);
                sumCount += read;

                if ( ( (eTime- sTime)/1000 ) >= 10 ) {

                    totalSubido.add(sumCount);
                    inputStream.close();
                    outputStream.close();

                    ftp.logout();
                    ftp.disconnect();
                    break;
                }


                //System.out.println("Percentace: " +" file: "+ sizeUpload +" total " + sumCount );

            }


        } catch (Exception e) {
            Log.e("Error: ", e.getMessage());
        }




        //InputStream inputStream = getResources().openRawResource(R.raw.random750x750);



    }




    // Async Task Class
    class testBandwidthDownload extends AsyncTask<String, String, String> {

        // Show Progress bar before downloading jpg
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Shows Progress Bar Dialog and then call doInBackground method
            progresBarEstatus.setProgress(0);
            progresBarEstatus.setMax(100);
            txtStatus.setText("Download");
            bntTest.setEnabled(false);

        }

        @Override
        protected String doInBackground(String... f_url) {
            int count = 0;
            try {


                /*

                //new ftp client
                FTPClient ftp = new FTPClient();
                //try to connect
                ftp.connect("ftp.sysdigital.com.ve");
                //login to server
                if (!ftp.login("mark@sysdigital.com.ve", "mark2015q")) {
                    ftp.logout();
                    //Toast.makeText(getApplicationContext(), "no se conecta", Toast.LENGTH_LONG).show();

                }

                ftp.setFileType(FTPSClient.BINARY_FILE_TYPE);
                ftp.enterLocalPassiveMode();
                ftp.setBufferSize(4096);


                //InputStream  inputStream = new FileInputStream("/sdcard/1mb.jpg");
                //OutputStream outputStream = ftp.storeFileStream("/1mb.jpg");

                //InputStream inputStream = ftp.storeFileStream("/1mb.jpg");
                //Boolean status1 = con.retrieveFile("/Baby", desFileStream1);

                //FileOutputStream fos = new FileOutputStream(localFilePath);
                //this.ftp.retrieveFile(remoteFilePath, fos);

                InputStream inputStream = ftp.retrieveFileStream("/1mb.jpg");
                FileOutputStream outputStream = new FileOutputStream("/sdcard/1mb.jpg");
                int lenghtOfFile = 1302245;


                byte[] bytesIn = new byte[4096];
                int read = 0;
                int bytesUploadedSet = 0;
                long startTime = System.currentTimeMillis();
                long endTime = 0;
                int pTotal;

                while((read = inputStream.read(bytesIn)) != -1) {
                      outputStream.write(bytesIn, 0, read);
                    bytesUploadedSet += read;
                    progresBarEstatus.setProgress((int) ((bytesUploadedSet * 100) / lenghtOfFile));
                    endTime = System.currentTimeMillis();
                    pTotal = (int) (( bytesUploadedSet * 8) / (endTime - startTime));
                    //this.publishProgress(String.valueOf(pTotal));
                }

                endTime = System.currentTimeMillis();
                downloadRate = ((((double) bytesUploadedSet * 8) / (endTime - startTime)));



                inputStream.close();
                outputStream.close();

                ftp.logout();
                ftp.disconnect();

                */



                /*

                InputStream input = new BufferedInputStream(url.openStream());

                OutputStream output = new FileOutputStream("/sdcard/1mb.jpg");



                String remoteFile2 = "/1mb.jpg";

                File downloadFile2 = new File("/sdcard/1mb.jpg");
                OutputStream outputStream2 = new BufferedOutputStream(new FileOutputStream(downloadFile2));
                InputStream inputStream = ftp.retrieveFileStream(remoteFile2);

                int size = inputStream.available();;

                byte[] bytesArray = new byte[1024];
                int bytesRead = -1;


                //byte data[] = new byte[1024];

                long total = 0;
                float bytesUploadedSet =0;

                long startTime = System.currentTimeMillis();
                long endTime = 0;

                while ((bytesRead = inputStream.read(bytesArray)) != -1) {
                    outputStream2.write(bytesArray, 0, bytesRead);
                    total += bytesRead;
                    //output.write(bytesArray, 0, bytesRead);
                    progresBarEstatus.setProgress( (int)((total*100)/lenghtOfFile) );
                }

                endTime = System.currentTimeMillis();
                downloadRate = ((((double) total * 8) / (endTime - startTime)));

                output.flush();
                output.close();
                input.close();

                ftp.logout();
                ftp.disconnect();

                Log.d("ANDRO_ASYNC", "Lenght of file: " + lenghtOfFile);
                Log.d("ANDRO_ASYNC", "Total Lenght of file: " + total);
                Log.d("ANDRO_ASYNC", "Time: " + (endTime - startTime));
                */




                //URL url = new URL("http://sysdigital.com.ve/mark/1mb.jpg");
                //URL url = new URL("http://200.26.135.54/speedtest/random1000x1000.jpg");
                //URL url = new URL("http://192.168.1.106/1mb.jpg");
                URL url = new URL("http://markmobile.brightcomms.com/static/filesDownload/random750x750.jpg");


                URLConnection conexion = url.openConnection();
                conexion.connect();


                int lenghtOfFile = conexion.getContentLength();
                //int lenghtOfFile = 1302245;

                InputStream input = new BufferedInputStream(url.openStream());
                OutputStream output = new FileOutputStream("/sdcard/1mb.jpg");

                byte data[] = new byte[1024];

                long total = 0;
                long pTotal;

                long startTime = System.currentTimeMillis();
                long endTime = 0;


                while ((count = input.read(data)) != -1) {
                    total += count;

                    progresBarEstatus.setProgress((int) ((total * 100) / lenghtOfFile));
                    output.write(data, 0, count);
                    // Partial conunt
                    endTime = System.currentTimeMillis();
                    pTotal = (long) (((double) total * 8) / (endTime - startTime));
                    this.publishProgress(String.valueOf(pTotal));
                }


                endTime = System.currentTimeMillis();

                double elapsedTimeSeconds = (System.currentTimeMillis() - startTime) / 1000;

                double readBandwidthUsed = (total*8) / elapsedTimeSeconds;

                double totalBit = total*8;
                double totalMbit = totalBit/1048;

                downloadRate = readBandwidthUsed;

                Log.d("ANDRO_ASYNC", "Lenght of file: " + lenghtOfFile);
                Log.d("ANDRO_ASYNC", "Total bit: " + totalBit);
                Log.d("ANDRO_ASYNC", "Total bit: " + totalMbit);
                Log.d("ANDRO_ASYNC", "Time seconds: " + elapsedTimeSeconds);
                Log.d("ANDRO_ASYNC", "Ancho de Banda: " + ((totalMbit/elapsedTimeSeconds)/1000) + " Mbps");


                output.flush();
                output.close();
                input.close();



            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }
            return null;
        }

        // While Downloading Music File
        protected void onProgressUpdate(String... progress) {
            // Set progress percentage
            //prgDialog.setProgress(Integer.parseInt(progress[0]));

            double value = Double.parseDouble(progress[0]);
            txtDownloadSpeed.setText(String.valueOf(round(value/1048,2)));
            speedometer.setSpeed(value/1048);
        }

        // Once Music File is downloaded
        @Override
        protected void onPostExecute(String file_url) {
            txtDownloadSpeed.setText(String.valueOf(round(downloadRate / 1048, 2)));
            txtStatus.setText("");
            new testBandwidthUpload().execute("URL");
        }
    }

    // Async Task Class
    class testBandwidthUpload extends AsyncTask<String, String, String> {

        // Show Progress bar before downloading Music
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progresBarEstatus.setProgress(0);
            progresBarEstatus.setMax(100);
            txtStatus.setText("Upload");
        }

        // Download Music File from Internet
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {

                //new ftp client
                FTPClient ftp = new FTPClient();
                //try to connect
                ftp.connect("ftp.sysdigital.com.ve");
                //login to server
                if (!ftp.login("mark@sysdigital.com.ve", "mark2015q")) {
                    ftp.logout();
                    //Toast.makeText(getApplicationContext(), "no se conecta", Toast.LENGTH_LONG).show();

                }

                ftp.setFileType(FTPSClient.BINARY_FILE_TYPE);
                ftp.enterLocalPassiveMode();
                ftp.setBufferSize(4096);

                // APPROACH #2: uploads second file using an OutputStream
                File secondLocalFile = new File("/sdcard/1mb.jpg");
                String secondRemoteFile = "/1mbX.jpg";
                InputStream inputStream = new FileInputStream(secondLocalFile);
                int lenghtOfFile = 1302245;

                System.out.println("Start uploading second file");
                OutputStream outputStream = ftp.storeFileStream(secondRemoteFile);
                byte[] bytesIn = new byte[4096];
                int read = 0;
                int bytesUploadedSet = 0;
                int pTotal = 0;

                long startTime = System.currentTimeMillis();
                long endTime = 1;

                while ((read = inputStream.read(bytesIn)) != -1) {
                    outputStream.write(bytesIn, 0, read);
                    bytesUploadedSet += read;
                    progresBarEstatus.setProgress((int) ((bytesUploadedSet * 100) / lenghtOfFile));
                    endTime = System.currentTimeMillis();
                   // if (endTime > startTime)
                    //    pTotal = (int) (( bytesUploadedSet * 8) / (endTime - startTime));
                    //this.publishProgress(String.valueOf(pTotal));
                }

                endTime = System.currentTimeMillis();
                long sec = TimeUnit.MILLISECONDS.toSeconds(endTime) - TimeUnit.MILLISECONDS.toSeconds(startTime);
                uploadRate = ( ((double) bytesUploadedSet) * 8) / sec;

                inputStream.close();
                outputStream.close();

                ftp.logout();
                ftp.disconnect();



                /*
                //new ftp client
                FTPClient ftp = new FTPClient();
                //try to connect
                ftp.connect("ftp.sysdigital.com.ve");
                //login to server
                if (!ftp.login("mark@sysdigital.com.ve", "mark2015q")) {
                    ftp.logout();

                }

                ftp.setFileType(FTPSClient.BINARY_FILE_TYPE);
                ftp.enterLocalPassiveMode();
                ftp.setBufferSize(1024000);

                OutputStream fout = null;
                InputStream bin = null;

                InputStream is = new FileInputStream("/sdcard/1mb.jpg");
                int lenghtOfFile = is.available();
                //int lenghtOfFile = 1302245;
                fout = ftp.storeFileStream("1mb_upload.jpg");

                bin = new FileInputStream("/sdcard/1mb.jpg");

                byte[] b = new byte[1024];
                int bytesRead = 0;
                float bytesUploadedSet = 0;

                long startTime = System.currentTimeMillis();
                long endTime = 0;

                while ((bytesRead = bin.read(b)) != -1) {
                    fout.write(b, 0, bytesRead);
                    bytesUploadedSet += bytesRead;
                    progresBarEstatus.setProgress((int) ((bytesUploadedSet * 100) / lenghtOfFile));
                }
                endTime = System.currentTimeMillis();
                uploadRate = ((((double) bytesUploadedSet) * 8) / (endTime - startTime));

                Log.d("ANDRO_ASYNC", "Lenght of file: " + lenghtOfFile);
                Log.d("ANDRO_ASYNC", "Total Lenght of file: " + bytesUploadedSet);
                Log.d("ANDRO_ASYNC", "Time: " + (endTime - startTime));

                fout.flush();
                fout.close();
                bin.close();

                ftp.logout();
                ftp.disconnect();
                */


            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }
            return null;
        }

        // While Downloading Music File
        protected void onProgressUpdate(String... progress) {
            // Set progress percentage
            double value = Double.parseDouble(progress[0]);
            txtUploadSpeed.setText(String.valueOf(round(value/1048,2)));
            speedometer.setSpeed(value / 1048);
            txtStatus.setText("");
        }

        // Once Music File is downloaded
        @Override
        protected void onPostExecute(String file_url) {
            txtUploadSpeed.setText(String.valueOf(round((uploadRate / 1048), 2)));
            txtStatus.setText("");
            bntTest.setEnabled(true);
        }
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }


}
